from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

CLIENTS = []

EVT_BROADCAST = 'broadcast event'
EVT_EMIT = 'normal emit event'
EVT_JSON = 'json event'
EVT_MSG = 'message event'
EVT_NUMBERS = 'numbers event'

NUMBERS_NAMESPACE = '/numbers'


@app.route('/')
def hello_world():
    return "You've hit a normal endpoint on our WebSocket test server."


@socketio.on(EVT_MSG)
def handle_message(message):
    print(
        'received {} event from client with message: {}'
        .format(EVT_MSG, message)
    )

    return '{} msg received'.format(EVT_MSG)


@socketio.on(EVT_JSON)
def handle_json(json):
    print(
        'received {} event from client with json: {}'
        .format(EVT_JSON, str(json))
    )

    return '{} msg received'.format(EVT_JSON)


@socketio.on(EVT_BROADCAST)
def handle_my_custom_event(json):
    print(
        'received {} event from client with message: {}'
        .format(EVT_BROADCAST, json)
    )
    print('server emit {} response'.format(EVT_BROADCAST))

    emit(
        EVT_BROADCAST,
        {'data': json},
        broadcast=True,
    )

@socketio.on(EVT_EMIT)
def handle_my_custom_event(message):
    print(
        'received {} event from client with message: {}'
        .format(EVT_EMIT, message)
    )
    print('server emit {} response'.format(EVT_EMIT))

    emit(
        EVT_EMIT,
        {'data': message},
    )


@socketio.on('connect', namespace=NUMBERS_NAMESPACE)
def test_connect():
    # need visibility of the global thread object
    global numbers_thread

    # do these here to avoid circular imports
    from gen_numbers import Numbers, thread_stop_event

    CLIENTS.append(request.sid)
    print(CLIENTS)
    print('Client connected')
    thread_stop_event.clear()
    #Start the random number generator thread only if the thread has not been started before.
    try:
        if not numbers_thread.isAlive():
            print("numbers_thread exists. Restart event loop...")
            numbers_thread.run()
    except NameError:
        print("numbers_thread did not exist. Starting...")
        numbers_thread = Numbers(socketio, NUMBERS_NAMESPACE, EVT_NUMBERS)
        numbers_thread.start()


@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')
    global numbers_thread
    from gen_numbers import thread_stop_event

    try:
        CLIENTS.remove(request.sid)
    except ValueError:
        pass

    if not CLIENTS:
        print('No more clients')
        thread_stop_event.set()


if __name__ == '__main__':
    app.debug = True
    socketio.run(app, host='0.0.0.0', port=5000, log_output=True)
