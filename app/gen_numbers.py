# Random Number Generator Thread
from gevent.threading import Thread
from gevent import sleep
from threading import Event
from random import random

numbers_thread = Thread()
thread_stop_event = Event()


class Numbers(Thread):
    def __init__(self, socketio, namespace, event_name):
        self.delay = 1
        self.namespace = namespace
        self.event_name = event_name
        self.socketio = socketio
        super().__init__()

    def random_number_generator(self):
        """
        Generate a random number every 1 second and emit to a socketio instance (broadcast)
        Ideally to be run in a separate thread?
        """
        print('Making random numbers')
        while True:
            if not thread_stop_event.isSet():
                number = round(random()*10, 3)
                print(number, self.namespace, self.event_name)

                self.socketio.emit(
                    self.event_name,
                    {'number': number},
                    namespace=self.namespace,
                )

            sleep(self.delay)

    def run(self):
        self.random_number_generator()
