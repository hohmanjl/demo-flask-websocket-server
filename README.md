# demo-flask-websocket-server

A server utility to demo/test WebSockets.
Try it with [Hermes WebSocket Client](https://chrome.google.com/webstore/detail/hermes-websocket-client/heiclpadccjamigpdpfpbdkcfnglcjgc) available in 
the Chrome Web Store.

|||
|---|---|
|Author|James Hohman|
|Repo|https://bitbucket.org/hohmanjl/demo-flask-websocket-server|

# Installation

```bash
virtualenv -p python3 ~/envs/flask-ws
source ~/envs/flask-ws/bin/activate
pip install -r requirements.txt
```

# Run the server

```bash
python app/app.py
```

Your server should now be live at http://localhost:5000/

# Connecting With Client

Point your client to url http://localhost:5000/socket.io

A number broadcast service will run when 
the client connects to the /numbers namespace.
Remember to subscribe to the "numbers event" in 
your client.

# Blog Post

* https://hohmanjl.wordpress.com/websockets-hermes-websocket-client

# References

* https://www.shanelynn.ie/asynchronous-updates-to-a-webpage-with-flask-and-socket-io/
* https://flask-socketio.readthedocs.io/en/latest/
* http://flask.pocoo.org/docs/1.0/quickstart/
* https://blog.miguelgrinberg.com/post/easy-websockets-with-flask-and-gevent
* https://github.com/socketio/socket.io-client/blob/master/docs/API.md
